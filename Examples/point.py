from typing import Generic, Tuple, TypeVar

T = TypeVar('T')
class Point(Generic[T]):
    """Implements operations over such geometric primitives as point and vector"""

    def __init__(self, x: T, y: T):
        if x is None or y is None:
            raise ValueError("Both (x, y) coordinates must be passed")
        self._x = x
        self._y = y

    def __str__(self):
        return "({0}, {1})".format(self._x, self._y)

    def __repr__(self):
        return "Point(x={0}, y={1})".format(self._x, self._y)

    def get(self) -> Tuple[T, T]:
        return self._x, self._y

    def __add__(self, other: 'Point') -> 'Point':
        return Point(self._x + other._x, self._y + other._y)

    def __sub__(self, other: 'Point') -> 'Point':
        return Point(self._x - other._x, self._y - other._y)

    def __neg__(self) -> 'Point':
        return Point(-self._x, -self._y)

    def __mul__(self, other: 'Point') -> T:
        """Scalar product"""
        return self._x * other._x + self._y + other._y

    def __mod__(self, other: 'Point') -> T:
        """Vector product"""
        return self._x * other._y - self._y * other._x

    def turn(self, ccw=True):
        return Point(-self._y, self._x) if ccw else Point(self._y, -self._x)


v = Point(2, 3)
u = Point(3.0, 4.0)
w = v + u
print(w)
print(v * u)
print(v % u)
print(v.turn())

print(dir(Point))
print(Point.__dict__)

# Интроспекция в python
import inspect

def get_my_code():
    x = "abcd"
    return x

print(inspect.getsource(get_my_code))