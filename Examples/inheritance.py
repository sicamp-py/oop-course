class Base:
    def __init__(self, value):
        self.value = value

    def base_method(self):
        print("base method")

    def get_base_or_derived(self):
        return 'Base'

class Derived(Base):
    def __init__(self, value):
        Base.__init__(self, value + 2)

    def get_base_or_derived(self):
        return 'Derived'

base = Base(12)
base.base_method()
print(base.get_base_or_derived())
print(base.value)

derived = Derived(92)
derived.base_method()
print(derived.get_base_or_derived())
print(derived.value)

#####################
#####################
class A:
    def __init__(self, int_val):
        self.val = int_val + 1

    @classmethod
    def fromString(cls, val):
        return cls(int(val))


class B(A):
    pass


x = A.fromString("1")
print(x.__class__.__name__) # A
x = B.fromString("1")
print(x.__class__.__name__) # B
z = A(10)
y = z.fromString(12)
print(y.val)

class D(object):
    @staticmethod
    def test(x):
        return x == 0

print(D.test(1)) # False
f = D()
print(f.test(0)) # True