def write(something, sep=' '):
    print(something + ":" + sep + str(eval(something)))

# examining an integer object
two = 2
write('two')
# two: 2

write("type(two)")
# type(two): <class 'int'>

write("type(type(two))")
# type(type(two)): class 'type'>

write("type(two).__bases__")
# type(two).__bases__: (<class 'object'>,)
print()

write("type('abc')")
# type('abc'): (<class 'str'>,)
write("type('abc').__bases__")
# type('abc').__bases__: (<class 'object'>,)
print()

write("type(write)")
# type(write): <class 'function'>
write("type(write).__bases__")
# type(write).__bases__: (<class 'object'>,)
print()

# !!! Все наследуются от object

write("object")
# object: <class 'object'>
write("type")
# type: <class 'type'>
print()

write("type(object)")
# type(object): <class 'type'>
write("object.__class__")
# object.__class__: <class 'type'>
print()

# type(z) и z.__class__ это одно и то же
# => class и type это синонимы

write("object.__bases__")
# object.__bases__: ()
print()

# У object'а нет родителя

write("type.__class__")
# type.__class__: <class 'type'>

# Класс type это он сам

write("type.__bases__")
# type.__bases__: (<class 'object'>,)
print()

# Родитель type это object

