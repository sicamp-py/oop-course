# Python Lexer Challenge

## Задание
Написать лексический анализатор для Python, который получает на вход текст программы и выдаёт список лексем (токенов).

Здесь можно почитать о лексической структуре Python: https://docs.python.org/3/reference/lexical_analysis.html

Заготовка для задания находится в каталоге `Challenge`

## Пример

### Input:
from zzz import qxx

### Output:
\[(TokenType.Keyword, source: "from"),<br>
 (TokenType.Whitespace, source: " "),<br>
 (TokenType.Identifier, source: "zzz"),<br>
 (TokenType.Whitespace, source: " "),<br>
 (TokenType.Keyword, source: "import"),<br>
 (TokenType.Whitespace, source: " "),<br>
 (TokenType.Identifier, source: "qxx")]
 