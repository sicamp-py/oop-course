from Challenge.token_type import TokenType


class Token:
    def __init__(self, token_type: TokenType, token_value: object, source: str):
        self.token_type = token_type
        self.token_value = token_value
        self.source = source

    def __str__(self):
        return '({0}, source: "{1}")'.format(self.token_type, self.source)
