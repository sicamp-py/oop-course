from enum import Enum


class TokenType(Enum):
    Keyword = 1,
    Identifier = 2,
    Whitespace = 3
