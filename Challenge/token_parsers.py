from Challenge.token import Token


class TokenParser:
    def try_parse(self, text: str) -> [Token, None]:
        raise NotImplementedError
