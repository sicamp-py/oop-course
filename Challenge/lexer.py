from typing import Iterator, List

from Challenge.token_parsers import *


class Lexer:
    def __init__(self, token_parsers: List[TokenParser], text: str):
        pass

    def has_token(self) -> bool:
        raise NotImplementedError

    def get_next_token(self) -> Token:
        raise NotImplementedError


def parse_program(text: str) -> Iterator[Token]:
    token_parsers = [...] # replace ... with your token parsers
    lexer = Lexer(token_parsers, text)
    while lexer.has_token():
        yield lexer.get_next_token()
